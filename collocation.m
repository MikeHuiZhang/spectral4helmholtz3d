% solve the Helmholtz equation defined on a cube
%   u_{xx} + u_{yy} + u_{zz} + k^2*u = f(x,y,z)
% with the second order absorbing conditions
%   u_n - 1i*k u - 0.5i/k \lap_t u= 0, on the faces
%   u_{n1}+u_{n2} - 1.5i*k u - 0.5i/k u_{tt} = 0, on the edges
%   u_{n1} + u_{n2} + u_{n3} - 2i*k u = 0 on the vertices,
% where 'n' is the outward normal direction, 't' is the tagential direction
clear all;

% wavenumber
k= 10; k2= k^2;

% geometry
xl= -1; yl= -1; zl= -1;
xr=  1; yr=  1; zr=  1; 

% source 
xs= xl + (xr-xl)/2;
ys= yl + (yr-yl)/2;
zs= zl + (zr-zl)/2; 
a= 1/2; 
r2= @(x,y,z) x.^2 + y.^2 + z.^2; 
fs= @(x,y,z) (r2(x,y,z)<=a^2).*exp(a^2./(r2(x,y,z)-a^2)); 

N= 20; % number of Chebyshev nodes is N+1
[A,x,y,z]= Assembly_Helmholtz(xl,xr,yl,yr,zl,zr,N,k2,k);
[X,Y,Z]= ndgrid(x,y,z);
fh= zeros(size(X)); idx= 2:N;
fh(idx,idx,idx)= fs(X(idx,idx,idx),Y(idx,idx,idx),Z(idx,idx,idx)); 
fh= fh(:);
uh= A\fh;

% uN20= load('k1N20','uh','x','y','z');
% uf= uN20.uh; xf= uN20.x; yf= uN20.y; zf= uN20.z; 
% disp('max |uh - uf| where uf is a fine solution');
% % uhf= polyfit3d(uh,N,x,y,z,xf,yf,zf);
% % interp3 accepts only outputs of meshgrid, the first output of meshgrid 
% % varies horizontally, but after reshape of u, its value varies with y 
% % horizontally. 
% [Yi,Xi,Zi]= meshgrid(linspace(yl,yr,101),linspace(xl,xr,101),linspace(zl,zr,101));
% [Yh,Xh,Zh]= meshgrid(y,x,z);
% [Yf,Xf,Zf]= meshgrid(yf,xf,zf);
% uh= reshape(uh,size(Xh));
% uf= reshape(uf,size(Xf));
% uhi= interp3(Yh,Xh,Zh,uh,Yi,Xi,Zi,'spline');
% ufi= interp3(Yf,Xf,Zf,uf,Yi,Xi,Zi,'spline');
% disp(max(abs(uhi(:)-ufi(:))));
