function [f,u]= getex2(X,Y,Z,k2)
% give an analytically exact example for 
%     f = (\lap + k2) u

% values at interior nodes
u= exp(X.*Y.^2.*Z.^3); 
Dxu= Y.^2.*Z.^3.*u;  
Dxxu= Y.^2.*Z.^3.*Dxu; 
Dyu= 2*Y.*X.*Z.^3.*u;
Dyyu= 2*Y.*X.*Z.^3.*Dyu + 2*X.*Z.^3.*u; 
Dzu= 3*Z.^2.*X.*Y.^2.*u;
Dzzu= 3*Z.^2.*X.*Y.^2.*Dzu + 6*Z.*X.*Y.^2.*u; 
f= Dxxu + Dyyu + Dzzu + k2*u;

% the second order absorbing boundary condition
k= sqrt(k2); 
f= setf4abc(f,k,u,Dxu,Dyu,Dzu,Dxxu,Dyyu,Dzzu);

% output column vectors
u= u(:);
f= f(:);

end