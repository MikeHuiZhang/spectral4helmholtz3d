function [A,x,y,z]= Assembly_Helmholtz(xl,xr,yl,yr,zl,zr,N,k2,k)
% assemble the matrix with the grid [xl,xr]*[yl,yr]*[zl,zr]
% for the Helmholtz equation defined on a cube
%   u_{xx} + u_{yy} + u_{zz} + k^2*u = f(x,y,z)
% with the second order absorbing conditions
%   u_n - 1i*k u - 0.5i/k \lap_t u= 0, on the faces
%   u_{n1}+u_{n2} - 1.5i*k u - 0.5i/k u_{tt} = 0, on the edges
%   u_{n1} + u_{n2} + u_{n3} - 2i*k u = 0 on the vertices,
% where 'n' is the outward normal direction, 't' is the tagential direction
% 

% without treatment of the boundary conditions
[Dx,Dy,Dz,x,y,z]= chebtocube(xl,xr,yl,yr,zl,zr,N);
Dxx= Dx^2; Dyy= Dy^2; Dzz= Dz^2;
Id= eye(N+1);
lap= kron(Id,kron(Id,Dxx)) + kron(Id,kron(Dyy,Id)) + kron(Dzz,kron(Id,Id));
mass= k2.*ones(N+1,N+1,N+1);
mass= spdiags(mass(:),0,size(lap,1),size(lap,2));
A= lap + mass;

% now treat the boundary conditions
SIZ= [N+1,N+1,N+1];
A= setallabc(A,SIZ,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);

end

