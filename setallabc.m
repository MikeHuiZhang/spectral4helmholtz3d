function A= setallabc(A,SIZ,k,Dx,Dy,Dz,Dxx,Dyy,Dzz)
%A= setallabc(A,SIZ,k,Dx,Dy,Dz,Dxx,Dyy,Dzz)
% set the matrix A for the grid of SIZ with the second-order a.b.c
% on all the boundary
ix= 2:SIZ(1)-1; iy= 2:SIZ(2)-1; iz= 2:SIZ(3)-1; 
A= setabc(A,SIZ,1,iy,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz); % face x==xl
A= setabc(A,SIZ,SIZ(1),iy,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);% face x==xr
A= setabc(A,SIZ,ix,1,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz); % face y==yl
A= setabc(A,SIZ,ix,SIZ(2),iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz); % face y==yr
A= setabc(A,SIZ,ix,iy,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);% face z==zl
A= setabc(A,SIZ,ix,iy,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz); % face z==zr
% edges along z-direction
A= setabc(A,SIZ,1,1,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),1,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,1,SIZ(2),iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),SIZ(2),iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
% edges along y-direction
A= setabc(A,SIZ,1,iy,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),iy,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,1,iy,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),iy,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
% edges along x-direction
A= setabc(A,SIZ,ix,1,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,ix,SIZ(2),1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,ix,1,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,ix,SIZ(2),SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
% vertices
A= setabc(A,SIZ,1,1,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,1,1,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,1,SIZ(2),1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,1,SIZ(2),SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),1,1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),1,SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),SIZ(2),1,k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
A= setabc(A,SIZ,SIZ(1),SIZ(2),SIZ(3),k,Dx,Dy,Dz,Dxx,Dyy,Dzz);
end