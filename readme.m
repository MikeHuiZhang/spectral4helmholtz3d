% README
% The main program is collocation.m, which uses Assembly_Helmholtz.m
% for assembly of the matrix.
% The following programs are used for testing and can be neglected:
%    testex.m, getex1.m, getex2.m, setf4abc.m.
% The program polyfit3d is for interpolating values between two different
% 3d grids. 
% The following programs serve for Assembly_Helmholtz.m:
%    cheb.m, chebtocube.m, setabc.m, setallabc.m.