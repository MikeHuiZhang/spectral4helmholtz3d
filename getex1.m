function [f,uexact]= getex1(X,Y,Z,k2)
% give an analytically exact example for 
%     f = (\lap + k2) uexact

% values at interior nodes
uexact= cos(X+2*Y+3*Z); 
Dxu= -sin(X+2*Y+3*Z); Dyu= -2*sin(X+2*Y+3*Z); Dzu= -3*sin(X+2*Y+3*Z);
Dxxu= -uexact; Dyyu= -4*uexact; Dzzu= -9*uexact; 
f= Dxxu + Dyyu + Dzzu + k2*uexact;

% the second order absorbing boundary condition
k= sqrt(k2); 
f= setf4abc(f,k,uexact,Dxu,Dyu,Dzu,Dxxu,Dyyu,Dzzu);

% output column vectors
uexact= uexact(:);
f= f(:);

end