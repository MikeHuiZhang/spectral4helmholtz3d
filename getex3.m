function [f,u]= getex3(X,Y,Z,k2)
% give an analytically exact example for 
%     f = (\lap + k2) u

% values at interior nodes
L= [1,2,3]; L= L/norm(L);
k= sqrt(k2);
u= exp(1i*k*(L(1)*X+L(2)*Y+L(3)*Z)); % a plane wave along the direction L
Dxu= 1i*k*L(1)*u; Dyu= 1i*k*L(2)*u; Dzu= 1i*k*L(3)*u;
Dxxu= -k2*L(1)^2*u; Dyyu= -k2*L(2)^2*u; Dzzu= -k2*L(3)^2*u; 
f= zeros(size(X));

% the second order absorbing boundary condition
f= setf4abc(f,k,u,Dxu,Dyu,Dzu,Dxxu,Dyyu,Dzzu);

% output column vectors
u= u(:);
f= f(:);

end