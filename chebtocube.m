function [Dx,Dy,Dz,x,y,z]= chebtocube(xl,xr,yl,yr,zl,zr,N)
% compute the differentiation matrix and Chebyshev nodes on [-1,1]
% by the program cheb.m from Treffethen's book 'Spectral methods in Matlab'
% and scale to the cube
[Dref,xref] = cheb(N); 
xref= flipdim(xref,1); % from -1 to 1
Dref= rot90(Dref,2);
% scale them to our geometry
Dx= Dref*2/(xr-xl); Dy= Dref*2/(yr-yl); Dz= Dref*2/(zr-zl);
x= xl + (xref+1)*(xr-xl)/2; 
y= yl + (xref+1)*(yr-yl)/2; 
z= zl + (xref+1)*(zr-zl)/2;