clear all;
disp('/ / / / / / / / / /');
disp('  new run of testex')
% wavenumber
k= 16; k2= k^2;

% geometry
xl= -1; yl= -1; zl= -1;
xr=  1; yr=  1; zr=  1; 

% solution on a fine grid
Nv= 4:16;
err= zeros(length(Nv),1);
for iN= 1:length(Nv) % number of Chebyshev nodes is N+1
    N= Nv(iN);
    [A,x,y,z]= Assembly_Helmholtz(xl,xr,yl,yr,zl,zr,N,k2,k);
    disp('max |A*uexact -f|')
    [X,Y,Z]= ndgrid(x,y,z);
    [f,uexact]= getex3(X,Y,Z,k2);
    disp(max(abs(A*uexact-f)));
    disp('max |A\f - uexact|')
    uh= A\f;
    err(iN)= max(abs(uh-uexact));
    disp(err(iN));
end
set(0,'defaultaxesfontsize',16); 
semilogy(Nv,err,'.-','MarkerSize',20)
xlabel('N','fontsize',16); 
ylabel('error','fontsize',16); 
title(['k=',num2str(k)],'fontsize',16);
% solution on a coarse grid
% N= 8;
% [A,xH,yH,zH]= Assembly_Helmholtz(xl,xr,yl,yr,zl,zr,N,k2,k);
% disp('max |A*uexact -f|')
% [X,Y,Z]= ndgrid(xH,yH,zH);
% [f,uexact]= getex3(X,Y,Z,k2);
% disp(max(abs(A*uexact-f)));
% disp('max |A\f - uexact|')
% uH= A\f;
% disp(max(abs(uH-uexact)));

% compare the solutions 
% uHh= polyfit3d(uH,N,xH,yH,zH,x,y,z);
% disp(max(abs(uHh-uh)));