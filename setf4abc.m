function f= setf4abc(f,k,uexact,Dxu,Dyu,Dzu,Dxxu,Dyyu,Dzzu)

[Nx,Ny,Nz]= size(uexact);
% -- faces
f(1,:,:)= -Dxu(1,:,:)-1i*k*uexact(1,:,:)-0.5i/k*(Dyyu(1,:,:)+Dzzu(1,:,:));
f(Nx,:,:)= Dxu(Nx,:,:)-1i*k*uexact(Nx,:,:)-0.5i/k*(Dyyu(Nx,:,:)+Dzzu(Nx,:,:));
f(:,1,:)= -Dyu(:,1,:)-1i*k*uexact(:,1,:)-0.5i/k*(Dxxu(:,1,:)+Dzzu(:,1,:));
f(:,Ny,:)= Dyu(:,Ny,:)-1i*k*uexact(:,Ny,:)-0.5i/k*(Dxxu(:,Ny,:)+Dzzu(:,Ny,:));
f(:,:,1)= -Dzu(:,:,1)-1i*k*uexact(:,:,1)-0.5i/k*(Dyyu(:,:,1)+Dxxu(:,:,1));
f(:,:,Nz)= Dzu(:,:,Nz)-1i*k*uexact(:,:,Nz)-0.5i/k*(Dyyu(:,:,Nz)+Dxxu(:,:,Nz));
% -- edges
% -- -- z-direction
f(1,1,:)= -Dxu(1,1,:)-Dyu(1,1,:)-1.5i*k*uexact(1,1,:)-0.5i/k*Dzzu(1,1,:);
f(1,Ny,:)= -Dxu(1,Ny,:)+Dyu(1,Ny,:)-1.5i*k*uexact(1,Ny,:)-0.5i/k*Dzzu(1,Ny,:);
f(Nx,1,:)= Dxu(Nx,1,:)-Dyu(Nx,1,:)-1.5i*k*uexact(Nx,1,:)-0.5i/k*Dzzu(Nx,1,:);
f(Nx,Ny,:)= Dxu(Nx,Ny,:)+Dyu(Nx,Ny,:)-1.5i*k*uexact(Nx,Ny,:)-0.5i/k*Dzzu(Nx,Ny,:);
% -- -- y-direction
f(1,:,1)= -Dxu(1,:,1)-Dzu(1,:,1)-1.5i*k*uexact(1,:,1)-0.5i/k*Dyyu(1,:,1);
f(1,:,Nz)= -Dxu(1,:,Nz)+Dzu(1,:,Nz)-1.5i*k*uexact(1,:,Nz)-0.5i/k*Dyyu(1,:,Nz);
f(Nx,:,1)= Dxu(Nx,:,1)-Dzu(Nx,:,1)-1.5i*k*uexact(Nx,:,1)-0.5i/k*Dyyu(Nx,:,1);
f(Nx,:,Nz)= Dxu(Nx,:,Nz)+Dzu(Nx,:,Nz)-1.5i*k*uexact(Nx,:,Nz)-0.5i/k*Dyyu(Nx,:,Nz);
% -- -- x-direction
f(:,1,1)= -Dyu(:,1,1)-Dzu(:,1,1)-1.5i*k*uexact(:,1,1)-0.5i/k*Dxxu(:,1,1);
f(:,1,Nz)= -Dyu(:,1,Nz)+Dzu(:,1,Nz)-1.5i*k*uexact(:,1,Nz)-0.5i/k*Dxxu(:,1,Nz);
f(:,Ny,1)= Dyu(:,Ny,1)-Dzu(:,Ny,1)-1.5i*k*uexact(:,Ny,1)-0.5i/k*Dxxu(:,Ny,1);
f(:,Ny,Nz)= Dyu(:,Ny,Nz)+Dzu(:,Ny,Nz)-1.5i*k*uexact(:,Ny,Nz)-0.5i/k*Dxxu(:,Ny,Nz);
% -- vertices
for ix=1:Nx-1:Nx
    signx= (ix==1)*(-1) + (ix==Nx);
    for iy= 1:Ny-1:Ny
        signy= (iy==1)*(-1) + (iy==Ny);
        for iz= 1:Nz-1:Nz
            signz= (iz==1)*(-1) + (iz==Nz);
            f(ix,iy,iz)= signx*Dxu(ix,iy,iz)+signy*Dyu(ix,iy,iz) ...
                + signz*Dzu(ix,iy,iz)-2i*k*uexact(ix,iy,iz);
        end
    end
end

end