function uhf= polyfit3d(uh,N,x,y,z,xf,yf,zf)
  uh= reshape(uh,N+1,(N+1)^2);
  uhf= zeros(length(xf),(N+1)^2);
  for iyz= 1:(N+1)^2
    F= polyfit(x,uh(:,iyz),N,domain([x(1),x(N+1)]));
    uhf(:,iyz)= F(xf);
  end
  uhfold= reshape(uhf, length(xf), N+1, N+1);
  uhf= zeros(length(xf),length(yf),N+1);
  for ix= 1:length(xf)
      for iz= 1:N+1
          F= polyfit(y,uhfold(ix,:,iz).',N,domain([y(1),y(N+1)]));
          uhf(ix,:,iz)= F(yf);
      end
  end
  uhfold= reshape(uhf, length(xf),length(yf),N+1);
  uhf= zeros(length(xf),length(yf),length(zf));
  for ix= 1:length(xf)
      for iy= 1:length(yf)
          F= polyfit(z,squeeze(uhfold(ix,iy,:)),N,domain([z(1),z(N+1)]));
          uhf(ix,iy,:)= F(zf);
      end
  end
  uhf= uhf(:);
  
end