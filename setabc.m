function A= setabc(A,SIZ,ix,iy,iz,k,Dx,Dy,Dz,Dxx,Dyy,Dzz)
%A= setabc(A,SIZ,ix,iy,iz,k)
% reset rows of A for the second order absorbing boundary condition
% A: In/Out 
%    the matrix
% SIZ: In 
%    the grids size consists of the numbers of nodes along x, y, z
% ix, iy, iz: In
%    the 1d indices of a face/edge/vertice, for one call input only one 
%    face/edge/vertice, the 3d indices of which are tensor product of 
%    ix,iy,iz 
% k: In
%    the wavenumber
% Dx, Dy, Dz, Dxx, Dyy, Dzz: In
%    the differentiation matrices for 1d grids

% Nearly no tensor available because k may be not separable.

% inputs check
if isempty(ix) || isempty(iy) || isempty(iz)
    return;
end
sizeA= size(A);
if sizeA(1) ~= sizeA(2)
    disp('setabc(): the input matrix A is not square');
    return;
end
if size(SIZ)<3
    disp(['setabc(): input SIZ must contain three integers for number of',...
        'grid points along x, y, z']);
end
if sizeA(1) ~= prod(SIZ(1:3))
    disp('setabc(): product of the input SIZ(1:3) is not equal to nrow of A');
    return;
end
if max(ix)>SIZ(1) || min(ix)<1
    disp('setabc(): input ix exceeds the 1~SIZ(1)');
    return;
end
if max(iy)>SIZ(2) || min(iy)<1
    disp('setabc(): input iy exceeds the input 1~SIZ(2)');
    return;
end
if max(iz)>SIZ(3) || min(iz)<1
    disp('setabc(): input iz exceeds the input 1~SIZ(3)');
    return;
end
if isnumeric(k) && ~isscalar(k)
    sizek= size(k);
    if prod(sizek(:))~=prod(SIZ(1:3))
        disp(['setabc(): input k is a (multi)vector but its length is', ...
            'not equal to product of SIZ(1:3)']);
        return;
    end
end
ix= unique(ix); iy= unique(iy); iz= unique(iz);
if length(ix)>1 && length(iy)>1 && length(iz)>1
    disp('setabc(): at least one of ix,iy,iz must be a scalar');
end
isboundary= 0;
if length(ix)==1 
    if ix==1 || ix==SIZ(1)
         isboundary= 1;
    end
end
if length(iy)==1
    if iy==1 || iy==SIZ(2)
        isboundary= 1;
    end
end
if length(iz)==1
    if iz==1 || iz==SIZ(3)
        isboundary= 1;
    end
end
if ~isboundary
    disp('setabc(): at least one of ix,iy,iz must be on the boundary');
end


% initialization
[IX,IY,IZ]= ndgrid(ix,iy,iz);
rowA= IX(:) + (IY(:)-1)*SIZ(1) + (IZ(:)-1)*SIZ(1)*SIZ(2);
A(rowA,:)= 0; 

% switch to the specific face/edge/vertice and set A
if length(ix)==1 && (1==ix || SIZ(1)==ix)
    signx= (ix==1)*(-1) + (ix==SIZ(1));
    if length(iy)==1 && (iy==1 || iy==SIZ(2))
        signy= (iy==1)*(-1) + (iy==SIZ(2)); 
        if length(iz)== 1 && (iz==1 || iz==SIZ(3)) % one of the eight vertices 
            signz= (iz==1)*(-1) + (iz==SIZ(3));
            r0 = 2;
            for irowA= 1:length(rowA)
                setDx();
                setDy();
                setDz();
                setD0();
            end
        else  % one of the edges along z-direction
            r0= 1.5;
            for irowA= 1:length(rowA)
                setDx();
                setDy();
                setD0();
                setDzz();
            end
        end
    elseif length(iz)==1 && (iz==1 || iz==SIZ(3)) % one the edges along y-direction
        signz= (iz==1)*(-1) + (iz==SIZ(3));
        r0= 1.5;
        for irowA= 1:length(rowA)
            setDx();
            setDz();
            setD0();
            setDyy();
        end
    else % a face x==xl or x==xr
        r0 = 1;
        for irowA= 1:length(rowA)
            setDx();
            setD0();
            setDyy();
            setDzz();
        end
    end
elseif length(iy)==1 && (iy==1 || iy==SIZ(2))
    signy= (iy==1)*(-1) + (iy==SIZ(2));
    if length(iz)==1 && (iz==1 || iz==SIZ(3)) % one of the edges along x-direction 
        signz= (iz==1)*(-1) + (iz==SIZ(3));
        r0= 1.5;
        for irowA= 1:length(rowA)
            setDy();
            setDz();
            setD0();
            setDxx();
        end    
    else  % a face y==yl, or y==yr
        r0= 1;
        for irowA= 1:length(rowA)
            setDy();
            setD0();
            setDxx();
            setDzz();
        end    
    end
elseif length(iz)==1 && (iz==1 || iz==SIZ(3)) % a face z==zl, or z==zr
    signz= (iz==1)*(-1) + (iz==SIZ(3));
    r0= 1;
    for irowA= 1:length(rowA)
        setDz();
        setD0();
        setDxx();
        setDyy();
    end
end

    function setDx()
        colA= (1:SIZ(1))+(IY(irowA)-1)*SIZ(1)+ (IZ(irowA)-1)*SIZ(1)*SIZ(2); 
        A(rowA(irowA),colA)= A(rowA(irowA),colA) + signx*Dx(ix,:);
    end

    function setDy()
        colA= IX(irowA) + (0:SIZ(2)-1)*SIZ(1) + (IZ(irowA)-1)*SIZ(1)*SIZ(2);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) + signy*Dy(iy,:);
    end

    function setDz()
        colA= IX(irowA) + (IY(irowA)-1)*SIZ(1) + (0:SIZ(3)-1)*SIZ(1)*SIZ(2);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) + signz*Dz(iz,:);
    end

    function setD0()
        A(rowA(irowA),rowA(irowA))= A(rowA(irowA),rowA(irowA))-r0*1i*k;
    end

    function setDyy()
        colA= IX(irowA) + (0:SIZ(2)-1)*SIZ(1) + (IZ(irowA)-1)*SIZ(1)*SIZ(2);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) -0.5i/k*Dyy(IY(irowA),:);
    end

    function setDzz()
        colA= IX(irowA) + (IY(irowA)-1)*SIZ(1) + (0:SIZ(3)-1)*SIZ(1)*SIZ(2); 
        A(rowA(irowA),colA)= A(rowA(irowA),colA) - 0.5i/k*Dzz(IZ(irowA),:);
    end

    function setDxx()
        colA= (1:SIZ(1)) + (IY(irowA)-1)*SIZ(1) + (IZ(irowA)-1)*SIZ(1)*SIZ(2);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) - 0.5i/k*Dxx(IX(irowA),:);
    end
end
